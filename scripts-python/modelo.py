import base64
import io
import numpy as np
from PIL import Image, ImageFilter
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras import Input, Model
from tensorflow.keras.layers import Dense, Conv2D, Flatten, LeakyReLU, BatchNormalization
from tensorflow.keras import backend
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt

mapa_etiquetas = {0: 'Diamante', 1: 'Ovalado', 2: 'Alargado', 3: 'Cuadrado', 4: 'Redondo', 5: 'Triángulo'}

def define_cnn():
    return load_model('modelo_cnn.h5')

def define_generator():
    return load_model("generator.h5")

def euclidean_distance_loss(y_true, y_pred):
    return backend.sqrt(backend.sum(backend.square(y_pred - y_true), axis = -1))

def define_encoder():  
    input_layer = Input(shape = (80, 80, 3))  
    enc = Conv2D(filters = 32, kernel_size = 5, strides = 2, padding = 'same')(input_layer)
    enc = LeakyReLU(alpha = 0.2)(enc)  
    enc = Conv2D(filters = 64, kernel_size = 5, strides = 2, padding = 'same')(enc)
    enc = BatchNormalization()(enc)
    enc = LeakyReLU(alpha = 0.2)(enc)
    enc = Conv2D(filters = 128, kernel_size = 5, strides = 2, padding = 'same')(enc)
    enc = BatchNormalization()(enc)
    enc = LeakyReLU(alpha = 0.2)(enc)
    enc = Conv2D(filters = 256, kernel_size = 5, strides = 2, padding = 'same')(enc)
    enc = BatchNormalization()(enc)
    enc = LeakyReLU(alpha = 0.2)(enc)
    enc = Flatten()(enc)
    enc = Dense(4096)(enc)
    enc = BatchNormalization()(enc)
    enc = LeakyReLU(alpha = 0.2)(enc)
    enc = Dense(100)(enc)
    model = Model(inputs = [input_layer], outputs = [enc])
    model.compile(loss = euclidean_distance_loss,
                    optimizer = 'adam')
    model.load_weights('encoder_w.h5')
    return model

def load_image(model_mtcnn, img_string):
    # decodificar imagen
    imagen = base64.b64decode(img_string)
    img = Image.open(io.BytesIO(imagen))
    if val_face(model_mtcnn, img):
        img_cnn = img.resize((178,178))
        img_gan = img.resize((80,80))
        # convert to array
        img_cnn = img_to_array(img_cnn)
        img_gan = img_to_array(img_gan)
        # reshape into a single sample with 3 channels
        img_cnn = img_cnn.reshape(1, 178, 178, 3)
        img_gan = img_gan.reshape(1, 80, 80, 3)
        # prepare pixel data
        img_cnn = img_cnn.astype('float32')
        img_gan = img_gan.astype('float32')
        img_cnn = img_cnn / 255.0
        img_gan = img_gan / 255.0
        return img_cnn, img_gan
    return None, None

def val_face(model_mtcnn, image):
    pixels = np.asarray(image)
    # detect face in the image
    faces = model_mtcnn.detect_faces(pixels)
    # skip cases where we could not detect a face
    if len(faces) == 0:
        return False
    else:
        return True

def plot_result(result_gen):
    result_gen = (result_gen + 1) / 2.0
    # plot images
    for i in range(6):
        # define subplot
        plt.subplot(2, 3, 1 + i).set_title(mapa_etiquetas[i])
        # turn off axis
        plt.axis('off')
        # plot raw pixel data
        plt.imshow(result_gen[i, :, :, :])
    buf = io.BytesIO()
    plt.savefig(buf, bbox_inches='tight', format='jpg')
    plt.close()
    buf.seek(0)
    img = Image.open(buf)
    im_result = img.filter(ImageFilter.SMOOTH)
    buffered = io.BytesIO()
    im_result.save(buffered, format="JPEG")
    img_str = base64.b64encode(buffered.getvalue())
    buf.close()
    return img_str
    
def gen_y():
    y = np.array([[1.,0.,0.,0.,0.,0.], [0.,1.,0.,0.,0.,0.]
                 ,[0.,0.,1.,0.,0.,0.],[0.,0.,0.,1.,0.,0.],[0.,0.,0.,0.,1.,0.],[0.,0.,0.,0.,0.,1.]])
    return y
def gen_x(result_ecoder):
    x = np.array([result_ecoder[0], result_ecoder[0]
                 ,result_ecoder[0],result_ecoder[0],result_ecoder[0],result_ecoder[0]])
    return x
 
def clasificar_generar(model_mtcnn, model_cnn, generator, encoder, img_string):
    img_cnn, img_gan = load_image(model_mtcnn=model_mtcnn, img_string=img_string)
    if img_cnn is None:
        print('no hay rostro')
        return {"resultado": "No se encontro ningún rostro en la imágen"}
    else:
        print('img_cnn ', img_cnn.shape, 'img_gan ', img_gan.shape)
        result_ecoder = encoder.predict(img_gan)
        y_batch = gen_y()
        x_batch = gen_x(result_ecoder)
        result_gen = generator.predict([x_batch, y_batch])
        result_img = plot_result(result_gen)
        result_cnn = model_cnn.predict(img_cnn)
        result_cnn = result_cnn * 100
        return {"resultadoImg": result_img.decode('utf-8'),"resultado": mapa_etiquetas[np.argmax(result_cnn)], "diamante": str(result_cnn[0][0]), "ovalado": str(result_cnn[0][1]), "alargado": str(result_cnn[0][2]), "cuadrado": str(result_cnn[0][3]), "redondo": str(result_cnn[0][4]), "triangulo": str(result_cnn[0][5])}





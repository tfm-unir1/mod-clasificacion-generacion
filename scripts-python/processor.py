import modelo
from mtcnn.mtcnn import MTCNN


class Processor:

   def __init__(self):
      self.model_mtcnn = MTCNN()
      self.model_cnn = modelo.define_cnn()
      self.generator = modelo.define_generator()
      self.encoder = modelo.define_encoder()
      

   def obtener_resultado(self, img_string):
      respuesta = modelo.clasificar_generar(self.model_mtcnn, self.model_cnn, self.generator, self.encoder, img_string)
      return respuesta




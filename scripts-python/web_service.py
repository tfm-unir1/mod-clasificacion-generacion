import cherrypy
import json
import processor
p = processor.Processor()

class WebService(object):

   @cherrypy.expose
   @cherrypy.tools.json_out()
   @cherrypy.tools.json_in()
   def clasificar_generar(self):
      try:
         data = cherrypy.request.json
         entrada = json.dumps(data)
         forma = p.obtener_resultado(data["imagenEntrada"])
         return {'respuesta':{'codigo':'1','descripcion':'OK'}, "forma":forma}
      except ValueError as ex:
         print(ex)
         return {'codigo':'3','descripcion':'ERROR'}

if __name__ == '__main__':
   config = {'server.socket_host': '192.168.100.16', 'server.socket_port': 8383}
   cherrypy.config.update(config)
   cherrypy.quickstart(WebService())
